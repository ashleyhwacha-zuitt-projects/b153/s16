/*function displayMsgToSelf(){

}

displayMsgToSelf();

let count = 10;

while(count !== 0){
	displayMsgToSelf();
	count--;
}
*/
// while loop allow us to repeat action or instruction as long as condition is true
/* count = 10
   
   1st loop - count 10
   2nd loop - count 9
   3nd loop - count 8
   4nd loop - count 7
   5nd loop - count 6
   6nd loop - count 5
   7nd loop - count 4
   8nd loop - count 3
   9nd loop - count 2
   10nd loop - count 1

   while loop checked if count is still NOT equal to 0
   at ths pt, before a possible 11th loop, count was decremented to 0, therefore there ws no 11th loop
*/

let numCount = 1;

while(numCount <= 5){
	console.log(numCount);
	numCount++;
}



let fruits = ["apple", "Durian","kiwi", "pineapple", "strawberry"];

console.log(fruits[0]);
console.log(fruits[fruits.length-1]);

// show all the items in an array in the console using a loop
for(let index=0; index < fruits.length; index++){
	console.log(fruits[index]);
};

// mini activity

// fav countries
let favCountries = ["Zimbabwe", "United States", "South Africa", "Phillipines", "Canada", "England"];

// for loop
for(let x=0; x < favCountries.length ; x++){
	console.log(favCountries[x]);
};

// if we want to assign
fruits[5] = "Grapes";
//console.log(fruits);

// Example of Array of objects

let cars =[
	{
	 brand: "Toyota",
	 type: "Sedan"
	},
	{
		brand: "Rolls Royce",
		type:"Luxury sedan"
	},
	{
		brand: "Mazda",
		type:"Hatchback"
	}
];

console.log(cars.length); //3 items

//console.log(cars[1]);

let myName = "adrIAn madArang";

for(let i=0; i < myName.length; i++){
     if(myName[i].toLowercase = "a" || myName[i].toLowercase= "e" || myName[i].toLowercase= "i" || myName[i].toLowercase = "o" || myName[i].toLowercase = "u"){
     	console.log(3);
     }else{
     	console.log("Non vowel "+ myName[i].toLowerCase);
     }
};


